from django.shortcuts import render


from datetime import datetime

from rest_framework import viewsets, status
from hotel.models import Room, Booking, RoomCategory
from .serializers import RoomSerializer, BookingSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response

# class RoomViewSet(viewsets.ReadOnlyModelViewSet):
#     queryset = Room.objects.all()
#     serializer_class = RoomSerializer
#
# class BookingViewSet(viewsets.ModelViewSet):
#     queryset = Booking.objects.all()
#     serializer_class = BookingSerializer

def create_booking_id(booking):
    return booking.room.room_number + str(booking.room.id)

def days_between_dates(date1, date2):
    date_format = "%Y-%m-%d"
    datetime1 = datetime.strptime(date1, date_format)
    datetime2 = datetime.strptime(date2, date_format)
    delta = datetime2 - datetime1
    return delta.days

@api_view(['GET'])
def booking_detail(request):
    booking_id = request.GET.get("booking_id")
    booking = Booking.objects.filter(booking_id=booking_id)
    serializer = BookingSerializer(booking, many=True)
    return Response(serializer.data)


@api_view(['POST'])
def book_room(request):
    room_type = request.POST.get("room_type")
    check_in_date = request.POST.get("check_in_date")
    check_out_date = request.POST.get("check_out_date")
    room_number = request.POST.get("room_number")


    total_booking_days = days_between_dates(check_in_date,check_out_date)

    if(total_booking_days < 0):
        return Response("Check in date cannot greater than checkout date", status=status.HTTP_200_OK)

    if(total_booking_days > 180):
        return Response("Cannot book room more than 6 months", status=status.HTTP_200_OK)

    room_category = RoomCategory.objects.get(name=room_type)

    room = Room.objects.get(category=room_category, room_number=room_number)

    booking = Booking.objects.filter(room=room,check_in_date__gte=check_in_date,check_in_date=check_in_date)
    if booking:
        return Response("Already booked")

    booking = Booking.objects.create(room=room,check_in_date=check_in_date,check_out_date=check_out_date)
    booking_id = create_booking_id(booking)
    booking.booking_id = booking_id
    booking.save()
    return Response("Booking Done", status=status.HTTP_201_CREATED)


@api_view(['GET'])
def check_room_availability(request):

    room_type = request.GET.get("room_type")
    check_in_date = request.GET.get("check_in")
    check_out_date = request.GET.get("check_out")

    room_category =  RoomCategory.objects.get(name=room_type)
    print(room_category, "room category")

    available_rooms = Room.objects.filter(category=room_category)
    print(available_rooms, "available room")

    booked_rooms = Booking.objects.filter(
        room__in=available_rooms,
        check_in_date__lte=check_out_date,
        check_out_date__gte=check_in_date
    )

    available_rooms = available_rooms.exclude(id__in=booked_rooms.values_list('room', flat=True))

    serializer = RoomSerializer(available_rooms, many=True)
    return Response(serializer.data)

