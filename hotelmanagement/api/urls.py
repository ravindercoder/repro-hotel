from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import  check_room_availability,book_room, booking_detail #, RoomViewSet, BookingViewSet,

# router = DefaultRouter()
# router.register(r'rooms', RoomViewSet)
# router.register(r'bookings', BookingViewSet)

urlpatterns = [
    # path('', include(router.urls)),
    path('book-room/', book_room),
    path('booking-detail/',booking_detail ),
    path('check-availability/', check_room_availability, name='check-availability'),
    # path('create-booking/', book_room, name='create-booking'),
]
