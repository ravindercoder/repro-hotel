from django.db import models

# Create your models here.

from django.db import models

class RoomCategory(models.Model):
    name = models.CharField(max_length=100)
    price_per_night = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return self.name

class Room(models.Model):
    category = models.ForeignKey(RoomCategory, on_delete=models.CASCADE)
    room_number = models.CharField(max_length=10)

    def __str__(self):
        return self.category.name + self.room_number


class Booking(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    check_in_date = models.DateField()
    check_out_date = models.DateField()
    booking_id = models.CharField(max_length=1000, unique=True)

    def __str__(self):
        return self.room.room_number

