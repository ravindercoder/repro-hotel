from django.urls import path
from .views import onetime

urlpatterns = [
    path('onetime/', onetime),
]
