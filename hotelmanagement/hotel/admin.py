from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import Booking, RoomCategory, Room


class BookingAdmin(admin.ModelAdmin):
    pass

class RoomCategoryAdmin(admin.ModelAdmin):
    pass

class RoomAdmin(admin.ModelAdmin):
    pass


admin.site.register(Booking, BookingAdmin)
admin.site.register(RoomCategory, RoomCategoryAdmin)
admin.site.register(Room, RoomAdmin)
