from django.shortcuts import render
from .models import *
from django.http import HttpResponse

# Create your views here.

# this for one time for creating sample datas
def onetime(request):
    deluxe_category = RoomCategory.objects.create(name='Deluxe Room', price_per_night=7000)
    luxury_category = RoomCategory.objects.create(name='Luxury Room', price_per_night=8500)
    suite_category = RoomCategory.objects.create(name='Luxury Suite', price_per_night=12000)
    presidential_category = RoomCategory.objects.create(name='Presidential Suite', price_per_night=20000)

    # Create rooms for each category
    for category in [deluxe_category, luxury_category, suite_category, presidential_category]:
        for i in range(1, 6):
            Room.objects.create(category=category, room_number=f'A{i:02d}')

    # Create a sample booking
    room = Room.objects.first()
    Booking.objects.create(room=room, check_in_date='2023-08-20', check_out_date='2023-08-25')
    return HttpResponse("bee")

